const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID
const settings = require('../../settings')

module.exports = class UserRepo {

    pluck(user, includePassword) {
        let u = Object.assign({}, user)
        
        if (!includePassword) {
            delete u.password
            delete u.salt
        }
        
        return u
    }
    
    async collection() {
        return (await MongoClient.connect(settings.databases.default)).collection('users')
    }

    async getUsers(filter, includePassword) {
        filter = filter || {}
        return (await (await this.collection()).find(filter).toArray()).map(u => this.pluck(u, includePassword))

    }

    async getUser(id, includePassword) {

        return this.pluck(await (await this.collection()).findOne({_id: ObjectId(id)}), includePassword)

    }

    async deleteUser(id) {

        return await (await this.collection()).findOneAndDelete({_id: ObjectId(id)})

    }

    async saveUser(user, includePassword) {
        let changeSet = Object.assign({}, user)
        
        delete changeSet.confirm_password
        delete changeSet._id

        if (!includePassword) {
            delete changeSet.password
            delete changeSet.salt
        }

        if (user._id) {
            let result = (await (await this.collection()).findOneAndUpdate({_id: ObjectId(user._id)}, {
                $set: changeSet
            }))
            return this.getUser(user._id)
        } else {
            return await this.getUser((await (await this.collection()).insertOne(changeSet)).ops[0]._id)
        }
    }

}