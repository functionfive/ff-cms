const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID
const settings = require('../../settings')

module.exports = class Setting {

    pluck(setting) {
        return {
            test: setting.test
        }
    }
    
    async collection() {
        return (await MongoClient.connect(settings.databases.default)).collection('settings')
    }

    async getSettings() {
        
        return await (await this.collection()).find({}).toArray()

    }

    async getSetting(id) {
        try {
            id = ObjectId(id)
            return await (await this.collection()).findOne({_id: id})
        } catch(err) { 
            console.log('getting setting ' + id)
            return await (await this.collection()).findOne({module: id.toUpperCase()})
        }
    }

    async getSettingByModule(module) {

        return await (await this.collection()).findOne({module: module})

    }

    async deleteSetting(id) {

        return await (await this.collection()).findOneAndDelete({_id: ObjectId(id)})

    }

    async saveSetting(setting) {
        if (setting._id) {
            let result = (await (await this.collection()).findOneAndUpdate({_id: ObjectId(setting._id)}, {
                $set: { 
                    module: setting.module,
                    values: setting.values
                }
            }))
            return this.getSetting(setting._id)
        } else {
            return (await (await this.collection()).insertOne({
                    module: setting.module,
                    values: setting.values
            })).ops[0]
        }
    }

}