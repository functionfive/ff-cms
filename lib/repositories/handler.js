const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID
const settings = require('../../settings')

module.exports = class Handler {

    pluck(handler) {
        return {
            test: handler.test
        }
    }
    
    async collection() {
        return (await MongoClient.connect(settings.databases.default)).collection('handlers')
    }

    async getHandlers() {
        
        return await (await this.collection()).find({}).toArray()

    }

    async getHandler(id) {
        try {
            id = ObjectId(id)
        } catch(err) { }
        return await (await this.collection()).findOne({_id: id})
    }

    async getHandlerByName(name) {

        return await (await this.collection()).findOne({name: name})

    }

    async deleteHandler(id) {

        try {
            id = ObjectId(id)
        } catch(err) { }

        return await (await this.collection()).findOneAndDelete({_id: id})

    }

    async saveHandler(handler) {
        let changeSet = Object.assign({}, handler)
        if (handler._id) {

            try {
                handler._id = ObjectId(handler._id)
            } catch(err) { }

            let result = (await (await this.collection()).findOneAndUpdate({_id: handler._id}, {
                $set: changeSet
            }))
            return this.getHandler(handler._id)
        } else {
            return (await (await this.collection()).insertOne({
                test: handler.test
            })).ops[0]
        }
    }

}