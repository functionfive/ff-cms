const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID
const settings = require('../../settings')
const HandlerRepo = require('./handler')

const handlerRepo = new HandlerRepo()

module.exports = class HandlerSource {

    pluck(source) {
        return {
            content: source.content,
            handlerId: source.handlerId
        }
    }
    
    async collection() {
        return (await MongoClient.connect(settings.databases.default)).collection('sources')
    }

    async getHandlerSources(handlerId) {
        
        return await (await this.collection()).find({handlerId: handlerId}, {version_number: 1, created_at: 1, handlerId: 1, _id: 1}).toArray()

    }

    async getHead(handlerId) {
        
        return (await (await this.collection()).find({handlerId: handlerId}).sort({version_number: -1}).limit(1).toArray())[0]

    }

    async getHeads() {
        let handlers = await handlerRepo.getHandlers()
        let heads = [];
        for( let handler of handlers) {
            heads.push(await this.getHead(handler._id.toString()))
        }
        return heads

    }

    async getHandlerSource(id) {

        return await (await this.collection()).findOne({_id: ObjectId(id)})

    }

    async deleteHandlerSource(id) {

        return await (await this.collection()).findOneAndDelete({_id: ObjectId(id)})

    }

    async saveHandlerSource(source) {
        if (source._id) {
            let result = (await (await this.collection()).findOneAndUpdate({_id: ObjectId(source._id)}, {
                $set: { 
                    test: source.test
                }
            }))
            return this.getHandlerSource(source._id)
        } else {
            return (await (await this.collection()).insertOne({
                test: source.test
            })).ops[0]
        }
    }

}