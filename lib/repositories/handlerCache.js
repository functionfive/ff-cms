const HandlerRepo = require('./handler')
const SourceRepo = require('./handlerSource')

let handlerRepo = new HandlerRepo()
let sourceRepo = new SourceRepo()

class HandlerCache {

    async refresh() {
        this.handlers = (await handlerRepo.getHandlers())
        this.sources = (await sourceRepo.getHeads())
        return true;
    }

    getHandlers() {
        
        return this.handlers

    }

    getHandler(id) {

        return this.handlers.find(h => h._id.toString() === id.toString())

    }

    getSourceHead(handlerId) {
        
        return this.sources.find(s => s.handlerId.toString() === handlerId.toString())

    }

    getHandlerByName(name) {
        
        return this.handlers.find(h => h.name === name)

    }

}
module.exports = new HandlerCache()