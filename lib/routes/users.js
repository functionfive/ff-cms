const express = require('express')
const UserRepo = require('../repositories/user')
const settings = require('../../settings')
const SettingsRepo = require('../repositories/settings')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const nodemailer = require('nodemailer')
const ObjectId = require('mongodb').ObjectID
const { adminOnly, authorize, passive } = require('./authorization')
const emailRegExp = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/

let userRepo = new UserRepo();
let settingsRepo = new SettingsRepo();
let router = express.Router()



router.get('/', adminOnly, async (req, res, next) => {
    try {
        res.send((await userRepo.getUsers()))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.get('/:id', authorize, async (req, res, next) => {
    try {

        if (req.user._id !== req.params.id && req.user.tags.indexOf('Administrator') === -1) {
            res.status(406).send({
                error: 'Not authorized'
            })
            return next()
        }

        res.send(await userRepo.getUser(req.params.id))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.put('/:id', authorize, async (req, res, next) => {
    try {
        let savePassword = false

        if (req.body.password && (!req.body.confirm_password || req.body.password !== req.body.confirm_password)) {
            res.status(400).send({
                errors: {
                    confirm_password: "Password confirmation is not valid"
                }
            })
            return next()
        }

        let existing = await userRepo.getUser(req.body._id)
        
        // Only administrators can update tags
        console.log(JSON.stringify(existing.tags) !== JSON.stringify(req.body.tags))
        console.log(req.body.tags)
        console.log(req.user.tags.indexOf('Administrator'))
        if (req.body.tags && JSON.stringify(existing.tags) !== JSON.stringify(req.body.tags) && req.user.tags.indexOf('Administrator') === -1) {
            res.status(406).send({
                error: "Not authorized"
            })
            return next()
        }

        if (!req.body.email || !emailRegExp.test(req.body.email)) {
            res.status(400).send({
                errors: {
                    email: "Email is invalid"
                }
            })
            return next()
        }

        if ((await userRepo.getUsers({email: req.body.email, _id: { $ne: ObjectId(req.body._id) }})).length > 0) {
            res.status(400).send({
                errors: {
                    email: "Email address already in use"
                }
            })
            return next()
        }

        // If they specify password then you need to get it nice and salty
        if (req.body.password) {
            savePassword = true
            req.body.salt = await bcrypt.genSalt()
            req.body.password = await bcrypt.hash(await bcrypt.hash(req.body.password, req.body.salt), settings.secret)
        }

        res.send(await userRepo.saveUser(req.body, savePassword))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.delete('/:id', adminOnly, async (req, res, next) => {
    try {
        res.send(await userRepo.deleteUser(req.params.id))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.post('/', passive, async (req, res, next) => {
    try {

        // if requesting user is not authenticated then force them to have no tags
        if (!req.user || !req.user.tags || req.user.tags.indexOf('Administrator') === -1) {
            req.body.tags = []
        }

        if (!req.body.confirm_password || req.body.password !== req.body.confirm_password) {
            res.status(400).send({
                errors: {
                    confirm_password: "Password confirmation is not valid"
                }
            })
            return next()
        }

        if (!req.body.email || !emailRegExp.test(req.body.email)) {
            res.status(400).send({
                errors: {
                    email: "Email is invalid"
                }
            })
            return next()
        }

        if ((await userRepo.getUsers({email: req.body.email})).length > 0) {
            res.status(400).send({
                errors: {
                    email: "Email address already in use"
                }
            })
            return next()
        }
        req.body.salt = await bcrypt.genSalt()
        req.body.password = await bcrypt.hash(req.body.password, req.body.salt)

        res.send(await userRepo.saveUser(req.body, true))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.post('/authorize', async (req, res, next) => {
    try {
        let users = await userRepo.getUsers({email: req.body.email}, true)
        let user = users.length > 0 ? users[0] : null
        
        if (!user || !(await bcrypt.compare(req.body.password, user.password))) {
            res.status(400).send({
                success: false,
                error: "Invalid credentials"
            })
            return next()
        }

        res.status(200).send({
            success: true,
            token: jwt.sign(userRepo.pluck(user), settings.secret, { expiresIn: '1h'})
        })
        next()

    } catch(err) {
        res.status(500).send({success: false, error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.post('/:id/password_reset_start', authorize, async (req, res, next) => {
    try {
        let user = await userRepo.getUser(req.params.id)
        
        if (!user) {
            res.status(404).send({
                success: false,
                error: 'User not found'
            })
            return next()
        }


        if (req.user._id !== req.params.id && req.user.tags.indexOf('Administrator') === -1) {
            res.status(406).send({
                error: 'Only administrators can reset other user\'s password'
            })
            return next()
        }

        let smtpSettings = await settingsRepo.getSettingByModule('SMTP')
        let adminSettings = await settingsRepo.getSettingByModule('ADMIN')
        console.log(smtpSettings, adminSettings)
        if (!smtpSettings) {
            res.status(400).send({
                success: false,
                error: 'SMTP settings are missing'
            })
            return next()
        }
        if (!adminSettings || !adminSettings.values || !adminSettings.values.password_reset) {
            res.status(400).send({
                success: false,
                error: 'Password reset settings are missing'
            })
            return next()
        }

        let reset_url = adminSettings.values.password_reset.reset_url.replace(':id', user.id).replace(':token', jwt.sign(user, settings.secret))

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport(smtpSettings.values);

        // setup email data with unicode symbols
        let mailOptions = {
            from: adminSettings.values.password_reset.sender, // sender address
            to: user.email, // list of receivers
            subject: adminSettings.values.password_reset.subject, // Subject line
            text: `A password reset has been requested. Go to ${reset_url} to reset your password`, // plain text body
            html: `
                <h2>Password Reset</h2>
                <p>A password reset has been requested.</p>
                <br><br>
                <a href="${reset_url}">Click here</a> to reset your password. If this was not initiated by you please disregard.
            ` // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.error(error)
                res.status(500).send({success: false, error: 'Server error', details: settings.debugging ? error.stack : null})
                return next()
            }
            res.status(200).send({
                success: true,
                token: jwt.sign(userRepo.pluck(user), settings.secret, { expiresIn: '1h'})
            })
            return next()
        });


    } catch(err) {
        res.status(500).send({success: false, error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.post('/:id/password_reset_complete', async (req, res, next) => {
    try {
        let payload = jwt.verify(req.body.token, settings.secret)
        let user = await userRepo.getUser(payload._id)
        
        if (!user) {
            res.status(404).send({
                success: false,
                error: 'User not found'
            })
            return next()
        }

        if (!req.body.confirm_password || req.body.password !== req.body.confirm_password) {
            res.status(400).send({
                errors: {
                    confirm_password: "Password confirmation is not valid"
                }
            })
            return next()
        }

        user.salt = await bcrypt.genSalt()
        user.password = await bcrypt.hash(req.body.password, user.salt)

        await userRepo.saveUser(user, true)

        res.send({success: true})
        next()

    } catch(err) {
        res.status(500).send({success: false, error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

module.exports = router