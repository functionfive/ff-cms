const express = require('express')
const HandlerRepo = require('../repositories/handler')
const settings = require('../../settings')
const { adminOnly, authorize, passive, authorizeTags } = require('./authorization')

let handlerRepo = new HandlerRepo();
let router = express.Router()

router.use('/:handlerId/source', (req, res, next) => {
    req.handler = {_id: req.params.handlerId}
    next()
}, require('./handlerSources'))



router.get('/', authorizeTags('Administrator', 'Author'), async (req, res, next) => {
    try {
        res.send(await handlerRepo.getHandlers())
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.get('/:id', authorizeTags('Administrator', 'Author'), async (req, res, next) => {
    try {
        res.send(await handlerRepo.getHandler(req.params.id))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.put('/:id', authorizeTags('Administrator', 'Author'), async (req, res, next) => {
    try {
        res.send(await handlerRepo.saveHandler(req.body))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.delete('/:id', authorizeTags('Administrator', 'Author'), async (req, res, next) => {
    try {
        res.send(await handlerRepo.deleteHandler(req.params.id))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

router.post('/', authorizeTags('Administrator', 'Author'), async (req, res, next) => {
    try {
        res.send(await handlerRepo.saveHandler(req.body))
        next()
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }
})

module.exports = router