const express = require('express')
const HandlerSourceRepo = require('../repositories/handlerSource')
const settings = require('../../settings')
const { adminOnly, authorize, passive, authorizeTags } = require('./authorization')

let sourceRepo = new HandlerSourceRepo();
let router = express.Router()

router.get('/', authorizeTags('Administrator', 'Author'), async (req, res) => {
    try {
        res.send(await sourceRepo.getHandlerSources(req.handler._id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.get('/head', authorizeTags('Administrator', 'Author'), async (req, res) => {
    try {
        res.send(await sourceRepo.getHead(req.handler._id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.get('/:id', authorizeTags('Administrator', 'Author'), async (req, res) => {
    try {
        res.send(await sourceRepo.getHandlerSource(req.params.id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.put('/:id', authorizeTags('Administrator', 'Author'), async (req, res) => {
    try {
        res.send(await sourceRepo.saveHandlerSource(req.body))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.delete('/:id', authorizeTags('Administrator', 'Author'), async (req, res) => {
    try {
        res.send(await sourceRepo.deleteHandlerSource(req.params.id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.post('/', authorizeTags('Administrator', 'Author'), async (req, res) => {
    try {
        res.send(await sourceRepo.saveHandlerSource(req.body))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

module.exports = router