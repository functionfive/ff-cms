const express = require('express')
const router = express.Router()
const HandlerRepo = require('../repositories/handler')
const SourceRepo = require('../repositories/handlerSource')
const handlerCache = require('../repositories/handlerCache')
const ejs = require('ejs')

const handlerRepo = new HandlerRepo();
const sourceRepo = new SourceRepo();

function processContent(handler, source) {

    switch(handler.type) {


        case 'EJS':
            return ejs.compile(source.content, {client: true})({}, null, (path, d) => {
                let h = handlerCache.getHandlerByName(path)
                let src = handlerCache.getSourceHead(h._id)
                return processContent(h, src)
            })

        default:
            return source.content
    }
}

router.use('*', async (req, res, next) => {
    // Get list of available handlers
    let handled = false;
    let handlers = handlerCache.getHandlers()
    handlers.forEach(async handler => {
        // find a match
        if (!handled && new RegExp(handler.routeMatcher).test(req.baseUrl)) {
            handled = true;
            console.log('Dynamic Route')
            
            // match found now go get its most recent source code variation
            let source = await sourceRepo.getHead(handler._id.toString())
            
            switch(handler.type) {
                case 'HTML':
                    res.set('content-type','text/html')
                    res.send(processContent(handler, source))
                    console.log(source._id)
                    next()
                break;
                case 'CSS':
                    res.set('content-type','text/css')
                    res.send(processContent(handler, source))
                    next()
                break;
                case 'ES5':
                    res.set('content-type','application/javascript')
                    res.send(processContent(handler, source))
                    next()
                break;
                case 'SOURCEMAP':
                    res.set('content-type','application/json')
                    res.send(processContent(handler, source))
                    next()
                break;
                case 'CUSTOM':
                    res.set('content-type', source.mime_type)
                    res.send(processContent(handler, source))
                    next()
                break;
                case 'EJS':
                    res.set('content-type', 'text/html')
                    res.send(processContent(handler, source))
                    next()
                break;
                case 'NODE':
                    let func = requireFromString(source.content, handler._id.toString())
                    func(req, res)
                    next()
                break;
            }
        }
    })
    if (!handled) next()
})

module.exports = router