const jwt = require('jsonwebtoken')
const settings = require('../../settings')

module.exports = class Authorization {


    static parseHeader(req) {
        let headerValue = req.headers.authorization
        let token = headerValue.split(' ')[1]
        req.user = jwt.verify(token, settings.secret)
    }


    static authorizeTags(...tags) {
        return async function authorize(req, res, next) {
            try {
                Authorization.parseHeader(req)

                if (tags.filter(t => (req.user.tags || []).indexOf(t) > -1).length === 0) {
                    return res.status(401).send({error: 'Not authorized'})
                }

                next()
            } catch(err) {
                console.error(err)
                if (err.name === 'TokenExpiredError') {
                    res.status(401).send({error: 'Token expired'})
                } else {
                    res.status(401).send({error: 'Not authorized'})
                }
            }
        }
    }


    static async adminOnly(req, res, next) {
        await (Authorization.authorizeTags('Administrator')(req, res, next))
    }

    
    static async passive(req, res, next) {
        try {
            Authorization.parseHeader(req)
            next()
        } catch(err) {
            next()
        }
    }


    static async authorize(req, res, next) {
        try {
            Authorization.parseHeader(req)
            next()
        } catch(err) {
            if (err.name === 'TokenExpiredError') {
                res.status(401).send({error: 'Token expired'})
            } else {
                res.status(401).send({error: 'Not authorized'})
            }
        }
    }


}