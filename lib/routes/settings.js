const express = require('express')
const SettingRepo = require('../repositories/settings')
const settings = require('../../settings')
const { adminOnly, authorize, passive, authorizeTags } = require('./authorization')

let settingRepo = new SettingRepo();
let router = express.Router()

router.get('/', authorizeTags('Administrator'), async (req, res) => {
    try {
        res.send(await settingRepo.getSettings(req.handler._id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.get('/:id', authorizeTags('Administrator'), async (req, res) => {
    try {
        res.send(await settingRepo.getSetting(req.params.id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.put('/:id', authorizeTags('Administrator'), async (req, res) => {
    try {
        res.send(await settingRepo.saveSetting(req.body))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.delete('/:id', authorizeTags('Administrator'), async (req, res) => {
    try {
        res.send(await settingRepo.deleteSetting(req.params.id))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

router.post('/', authorizeTags('Administrator'), async (req, res) => {
    try {
        res.send(await settingRepo.saveSetting(req.body))
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
    }
})

module.exports = router