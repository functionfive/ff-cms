const express = require('express')
const settings = require('../../settings')
const handlerCache = require('../repositories/handlerCache')
const MongoClient = require('mongodb').MongoClient
const { adminOnly, authorize, passive } = require('./authorization')

const uuid = require('uuid')
const fs = require('fs')
let router = express.Router()

if (!fs.existsSync('./tmp'))
    fs.mkdirSync('./tmp')
const Git = require('simple-git')('./tmp')

router.get('/install', async (req, res, next) => {
    try {
        let db = await MongoClient.connect(settings.databases.default)
        let override = req.query.override && req.query.override === true || req.query.override.toLowerCase() === 'true' ? true : false
        console.log('cloning ' + req.query.repo)
        let id = uuid.v4().substr(0,8)
        await Git.clone(req.query.repo, './' + id)

        let installer = require('../../tmp/' + id + '/dist/pkg/install')

        await installer(db, override)
        await handlerCache.refresh()
        
        res.send({
            success: true
        }) 
    } catch(err) {
        res.status(500).send({error: 'Server error', details: settings.debugging ? err.stack : null})
        console.error(err)
        next()
    }

})

module.exports = router