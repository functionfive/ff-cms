function _invalidateRequireCacheForFile(filePath){
	console.log('invalidating cache ' + require.resolve(filePath))
    delete require.cache[require.resolve(filePath)];
};

function requireNoCache(filePath) {
	_invalidateRequireCacheForFile(filePath);
	return require(filePath);
};

function requireFromString(src, filename) {
  var Module = module.constructor;
  var m = new Module();
  m._compile(src, filename);
  return m.exports;
}

module.exports.requireNoCache = requireNoCache;
module.exports.requireFromString = requireFromString