const requireFromString = require('./lib/requireExtensions').requireFromString
const express = require('express')
const bodyParser = require('body-parser')
const handlerCache = require('./lib/repositories/handlerCache')
const cors = require('cors')

const app = express()
const PORT = process.env.PORT || 3000

app.use(cors())
app.use(bodyParser.json())

app.use('*', require('./lib/routes/dynamicRoutes'))

app.use('/api/handlers', require('./lib/routes/handlers'))
app.use('/api/users', require('./lib/routes/users'))
app.use('/api/modules', require('./lib/routes/modules'))
app.use('/api/settings', require('./lib/routes/settings'))

app.use('*', async (req, res, next) => {
    console.log(`[${req.method}] ${req.baseUrl} - ${res.statusCode}`)
    next()
})

console.log('Loading Handlers Cache')
handlerCache.refresh().then(() => {
    app.listen(PORT, function () {
        console.log(`Example app listening on port ${PORT}!`)
    })
}).catch(err => {
    console.error(err)
    process.exit(1)
})
