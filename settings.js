if (!process.env.SECRET) {
    throw new Error('Missing SECRET in environment!')
}

module.exports = {

    secret: process.env.SECRET,

    debugging: typeof process.env.DEBUG === 'undefined' ? false : process.env.DEBUG,

    databases: {
        default: process.env.DB_URL || 'mongodb://127.0.0.1:27017/ffive-cms'
    }

}
